#include<iostream>
#include<cstdlib>
#include"Queue.h"
#include"Stack.h"

using namespace std;

int main()
{
	int size;
	int select = 0;

	cout << "ENTER SIZE" << endl;
	cin >> size;
	Stack<int>Stack(size);
	Queue<int>Queue(size);

	while (true)
	{
		cout << R"( SELECT AN ACTION
					1 - PUSH
					2 - POP
					3 - PRINT ALL AND THEN EMPTY
					4 - EXIT )" << endl;
		cin >> select;

		if (select == 1)
		{
			int add;
			cout << "HOW MANY NUMBERS WOULD YOU LIKE TO ADD" << endl;
			cin >> add;

			Queue.push(add);

			Stack.push(add);
		
		}
		
		else if (select == 2)
		{
			cout << "YOU POPPED THE FRONT ELEMENTS" << endl;
			Queue.pop();

			Stack.pop();

			system("pause");

		}

		else if (select == 3)
		{
			cout << "DISPLAY ALL ELEMENTS" << endl;
			cout << "QUEUE:" << endl;
			Queue.print();

			cout << "STACK:" << endl;
			Stack.print();

			system("pause");
			cout << "NOW CLEARING" << endl;
			Queue.clear();
			Stack.clear();
			system("pause");
		}
		else if (select == 4)
		{
			return false;
		}


		cout << "TOP ELEMENTS OF SET" << endl;

		cout << "QUEUE: " << Queue.top() << endl;
		cout << "STACK: " << Stack.top() << endl;

		system("pause");
		system("cls");
	} 
	
	system("pause");
	return 0;

}
